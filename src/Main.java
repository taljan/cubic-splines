import dto.Polynomial;
import io.MeasuringPointReader;
import math.Gauss;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        String filename = "Data.txt";

        MeasuringPointReader reader = new MeasuringPointReader(filename);
        reader.readFile();

        List<Point2D> dataList = reader.getSortedMeasuringPoints();

        List<Double> interval = reader.getInterval();
        double intervalStart = findMinValue(interval);
        double intervalEnd = findMaxValue(interval);

        List<Polynomial> polynomials = createPolynomialListFromDataList(dataList, intervalStart, intervalEnd);
        createAndSolveSystemOfLinearEquations(polynomials);
    }

    private static List<Polynomial> createPolynomialListFromDataList(List<Point2D> dataList, double intervalStart,
            double intervalEnd) {

        List<Polynomial> polynomials = new ArrayList<>();

        for (Point2D dataPoint : dataList) {
            boolean hasDataPointSharedPolynomial = dataPoint.getX() != intervalStart && dataPoint.getX() != intervalEnd;

            if (hasDataPointSharedPolynomial) {
                createAndAddPolynomial(dataPoint, polynomials, true);
            } else {
                createAndAddPolynomial(dataPoint, polynomials, false);
            }
        }

        return polynomials;
    }

    private static void createAndSolveSystemOfLinearEquations(List<Polynomial> polynomials) {
        double[][] systemOfLinearEquation = new double[polynomials.size()][polynomials.get(0).getDeg()];

        fillSystemOfLinearEquationWithPolynomials(polynomials, systemOfLinearEquation);
        double[] yValues = retrieveYValuesFromPolynomials(polynomials, systemOfLinearEquation);

        Gauss.getSolution(systemOfLinearEquation, yValues, true);
    }

    private static void fillSystemOfLinearEquationWithPolynomials(List<Polynomial> polynomials,
            double[][] systemOfLinearEquation) {

        for (int i = 0; i < systemOfLinearEquation.length; i++) {
            systemOfLinearEquation[i] = polynomials.get(i).getCoeffs();
        }
    }

    private static double[] retrieveYValuesFromPolynomials(List<Polynomial> polynomials,
            double[][] systemOfLinearEquation) {

        double[] yValues = new double[systemOfLinearEquation.length];

        for (int i = 0; i < systemOfLinearEquation.length; i++) {
            yValues[i] = polynomials.get(i).getY();
        }

        return yValues;
    }

    private static void createAndAddPolynomial(Point2D dataPoint, List<Polynomial> polynomials,
            boolean hasDataPointSharedPolynomial) {

        Polynomial polynomial = new Polynomial(dataPoint.getX(), dataPoint.getY());
        polynomials.add(polynomial);

        Polynomial derivative = polynomial.getDerivation();
        polynomials.add(derivative);

        if (hasDataPointSharedPolynomial) {
            Polynomial sharedPolynomial = new Polynomial(dataPoint.getX(), dataPoint.getY());
            polynomials.add(sharedPolynomial);

            Polynomial sharedDerivative = sharedPolynomial.getDerivation();
            polynomials.add(sharedDerivative);
        }
    }

    private static double findMinValue(List<Double> interval) {
        if (interval == null || interval.size() == 0) {
            return Double.MAX_VALUE;
        }

        List<Double> sortedInterval = new ArrayList<>(interval);
        Collections.sort(sortedInterval);

        return sortedInterval.get(0);
    }

    private static double findMaxValue(List<Double> interval) {
        if (interval == null || interval.size() == 0) {
            return Double.MAX_VALUE;
        }

        List<Double> sortedInterval = new ArrayList<>(interval);
        Collections.sort(sortedInterval);

        return sortedInterval.get(sortedInterval.size() - 1);
    }
}
