package dto;

import java.util.Arrays;

public class Polynomial {

    private int deg;

    private double[] coeffs;

    private double y;
    private double x;

    private Polynomial derivation;

    private Polynomial(double x, double y, int deg) {
        this.x = x;
        this.y = y;
        this.deg = deg;
        this.coeffs = new double[deg + 1];
    }

    public Polynomial(double x, double y) {
        this.x = x;
        this.y = y;
        this.deg = 3;
        this.coeffs = new double[deg + 1];

        calcCoeffs();
        derivation = calcDerivation();
    }

    private Polynomial calcDerivation() {
        Polynomial derivative = new Polynomial(x, y, deg);
        derivative.setY(0);

        double[] newCoeffs = new double[deg + 1];

        newCoeffs[0] = 3 * Math.pow(x, 2);
        newCoeffs[1] = 2 * Math.pow(x, 1);
        newCoeffs[2] = 1 * Math.pow(x, 0);
        newCoeffs[3] = 0.0;

        derivative.setCoeffs(newCoeffs);

        return derivative;
    }

    private void calcCoeffs() {
        coeffs[0] = Math.pow(x, 3);
        coeffs[1] = Math.pow(x, 2);
        coeffs[2] = Math.pow(x, 1);
        coeffs[3] = Math.pow(x, 0);
    }

    @Override
    public String toString() {
        return "Polynomial{" + "coeff=" + Arrays.toString(coeffs) + ", y=" + y + '}';
    }

    public Polynomial getDerivation() {
        return derivation;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setCoeffs(double[] coeffs) {
        this.coeffs = coeffs;
    }

    public double getY() {
        return y;
    }

    public double[] getCoeffs() {
        return coeffs;
    }

    public int getDeg() {
        return deg;
    }
}
