package io;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MeasuringPointReader {
    private String fileName;
    private BufferedReader bufferedReader;
    private List<Point2D> measuringPoints;
    private List<Double> intervalPoints;

    public MeasuringPointReader(String filename) {
        this.fileName = filename;
        measuringPoints = new ArrayList<>();
        intervalPoints = new ArrayList<>();
    }

    /**
     * reads files using a BufferedReader handing over lines for interpretation
     */
    public void readFile() {
        String currentLine;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((currentLine = bufferedReader.readLine()) != null) {
                interpretLine(currentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * interprets line aka splits on agreed delimiters adds resulting point to list
     *
     * @param line
     */
    private void interpretLine(String line) {
        String[] lineSplitted = line.split(";");

        for (String s : lineSplitted) {
            String[] pointSplitted = s.split(",");

            double x = Double.parseDouble(pointSplitted[0]);
            double y = Double.parseDouble(pointSplitted[1]);

            Point2D tmp = new Point2D.Double(x, y);

            measuringPoints.add(tmp);
            intervalPoints.add(x);
        }
    }

    public List<Point2D> getSortedMeasuringPoints() {
        return this.sortMeasuringPoints();
    }

    public List<Double> getInterval() {
        return this.intervalPoints;
    }

    /**
     * sorts measurement points based on bubble sort algorithm
     *
     * @return ArrayList<Point2D>
     */
    private ArrayList<Point2D> sortMeasuringPoints() {
        Point2D[] pointsAsArray = new Point2D[measuringPoints.size()];
        int index = 0;

        for (Point2D point : measuringPoints) {
            pointsAsArray[index] = point;
            index++;
        }

        for (int i = 0; i < pointsAsArray.length - 1; i++) {
            for (int j = 0; j < pointsAsArray.length - i - 1; j++) {
                if (pointsAsArray[j].getX() > pointsAsArray[j + 1].getX()) {
                    Point2D temp = pointsAsArray[j];
                    pointsAsArray[j] = pointsAsArray[j + 1];
                    pointsAsArray[j + 1] = temp;
                }
            }
        }

        ArrayList<Point2D> sortedMeasuringPoints = new ArrayList<>();

        for (Point2D point : pointsAsArray) {
            sortedMeasuringPoints.add(point);
        }

        return sortedMeasuringPoints;
    }
}
