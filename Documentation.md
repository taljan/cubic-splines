# Dokumentation

### How to
1. Projekt Clonen.
   
   Im Terminal deiner Wahl folgendes eingeben:
   git clone git@bitbucket.org:taljan/cubic-splines.git
   
**Falls das clonen nicht klappt, einfach die Sourcefiles (mitgeschickt in der Mail) entzippen und in einer IDE öffnen.**


2. Projekt in einer IDE öffnen.
3. Main Methode Starten.
4. Konsole analysieren.

### Struktur des Projektes
   * drei Packages:
        1. io - Package: enthält Klassen zum Einlesen und Interpretieren der Datei
           * Die Daten werden von einer .txt Datei eingelesen und in eine Liste gespeichert.
           * In der Liste befinden sich lauter Point2D Objekte -> (x, y) Werte.
        2. math - Package: enthält die Klasse zum Lösen eines LGS anhand von Gauss Jordan Algorithmus.
           * Die einzelnen Schritte des Gauß-Jordan werden im JavaDoc genauer erläutert.
        3. dto - Package: enthält data transfer object (DTO), welche die Daten eines Polynoms modellieren.
     4.  default (src folder) - Package: enthält main-Klasse mit der main Methode.
     5.  Im Projektordner selbst befindet sich auch die Datei mit den vorgegebenen Messpunkten.


### Vorgehen
   * Das Programm nimmt die Daten von der .txt Datei und liest jede Zeile einzeln ein. Die Datenpunkte werden mit einem regulären Ausdruck sinngemäß getrennt, sodass die Kommata und Semikolons verschwinden und die Punkte in ein **Point2D** Objekt gespeichert werden können.
   * Der Intervallbereich der Datenpunkte wird ermittelt. Somit wissen wir welche Punkte wie oft in ein Polynom hinzugefügt werden muss. Die Punkte die sich im Intervall befinden müssen doppelt genommen werden.
   * Nachdem wir die Polynome, je nach Position im Intervall, bestimmt haben, könne wir diese inklusive deren ersten Ableitung und y-Werten in eine Liste speichern.
   * Mithilfe der Liste können wir nun ein LGS erstellen.
```java
   +1.00    +1.00    +1.00    +1.00    |    +1.00
   +3.00    +2.00    +1.00    +0.00    |    +0.00
   +8.00    +4.00    +2.00    +1.00    |    +4.00
   +12.00   +4.00    +1.00    +0.00    |    +0.00
   +8.00    +4.00    +2.00    +1.00    |    +4.00
   12.00    +4.00    +1.00    +0.00    |    +0.00
   +27.00   +9.00    +3.00    +1.00    |    +3.00
   +27.00   +6.00    +1.00    +0.00    |    +0.00
```
   * Jetzt müssen lediglich das LGS lösen.
     * Die einzelnen Schritte können in der Konsole aufgezeigt werden, indem man in der Funktion die richtigen Parameter übergibt.
   * Nach den Berechnungen bekommen wir folgendes Ergebnis für die drei vorgegebenen Punkte.

```java
   +1.00    +0.00    +0.00    +0.00    |    -6.00
   +0.00    +1.00    +0.00    +0.00    |    +27.00
   +0.00    +0.00    +1.00    +0.00    |    -36.00
   +0.00    +0.00    +0.00    +1.00    |    +16.00
   +0.00    +0.00    +0.00    +0.00    |    +0.00
   +0.00    +0.00    +0.00    +0.00    |    +0.00
   +0.00    +0.00    +0.00    +0.00    |    +14.00
   +0.00    +0.00    +0.00    +0.00    |    +36.00
```

Wir bekommen davon die Koeffizienten unseren gesuchten Polynoms.
```math
f(x) = 36x^7 + 14x^6 + 0x^5 + 0x^4 + 16x^3 - 36x^2 + 27x - 6
```

Rechenweg in der Konsole anzeigen. (default auf ***true***)
  ```java
  Gauss.getSolution(systemOfLinearEquation, yValues, true);
  ```

Rechenweg in der Konsole ***nicht*** anzeigen.
```java
  Gauss.getSolution(systemOfLinearEquation, yValues, false;
  ```

### Limitierung
   * ausschließlich für eindeutige LGS.

### Ausblick
   * Grafische Darstellung der Polynome.
   * Verschiebung der Punkte in einem UI, insbesondere der Randpunkte.
      * Um sowas allerdings realisieren zu können reicht ein einfaches Java Framework zum plotten der Daten nicht aus. Um dieses Problem zu lösen, gibt es zwei Möglichkeiten.

   **1. Wir trennen "Datenlogik" und "Visualisierungslogik"**
   * Das bedeutet, dass wir durch einen kleinen Webserver, dieser Webserver kann auch lokal auf der eigenen Maschine laufen, die Daten an eine Webseite senden.
   * Diese Webseite kann dann mit herkömmlichem Javascript die Daten direkt plotten. Dies hat den großen Vorteil, dass wir bereits vorhanden Frameworks nutzen und direkt implementieren können.

   **2. Wir nutzen Python**
   * Python bietet von Haus direkt solche Frameworks zum visualisieren und berechnen an. (Dafür wurde Python entwickelt)
   * Wir brauchen also keine Webserver mehr und müssen keine Logik voneinander trennen.
   * Das erlernen der Syntax ist natürlich erforderlich.

